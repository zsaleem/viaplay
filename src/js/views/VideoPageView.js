define(function (require) {

    var VideoPageTemplate = require('text!../../templates/video.html'),
        ErrorTemplate = require('text!../../templates/error.html'),
        $ = require('jquery'),
        _ = require('underscore');
    
    var VideoPageView = function (data, model) {
        this._data = data;
        this._model = model;
    };

    VideoPageView.prototype = (function () {

        var template = _.template(VideoPageTemplate),
            error = _.template(ErrorTemplate),
            id = null,
            movieDetails = {};
        
        var render = function () {
            var self = this;

            this._model.getTrailer(this._data.imdbID).done(function (data) {                
                 self._data['Clip'] = 'http://www.imdb.com/title/' + self._data.imdbID;  // data.clips[0].links.alternate;

                $('.wrapper').html(template( self._data ));
            }).error(function (error) {
                $('.wrapper').html(error({ status: error.status, text: error.statusText }));
            });
        };

        return {
            render: render
        };

    })();

    return VideoPageView;
});

