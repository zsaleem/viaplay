/* global define:false */
define(function (require) {

    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
        VideoModel = require('models/VideoPageModel'),
        VideoController = require('controllers/VideoPageController'),
        VideoView = require('views/VideoPageView'),
        ErrorTemplate = require('text!../templates/error.html');

    var App = (function () {

        var template = _.template(ErrorTemplate);

        var init = function () {
            var model = new VideoModel();
            var view, controller;

            model.get().done(function (data) {
                view = new VideoView(data, model, {});
                controller = new VideoController(model, view);
                view.render();
            }).fail(function (error) {
                console.log(error);
                $('.wrapper').html(template({ status: error.status, text: error.statusText }));
            });
        };

        return {
            init: init
        };

    }());

    return App;
});
