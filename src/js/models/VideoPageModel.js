define(function (require) {

    'use strict';

    var Proxy = require('../sandbox/Proxy');

    var VideoPageModel = function () {
        this._proxy = new Proxy();
    };

    VideoPageModel.prototype = (function () {
        
        var get = function () {
            return this._proxy.getDetails();
        },

        getTrailer = function (id) {
            return this._proxy.getTrailer(id);
        };

        return {
            get: get,
            getTrailer: getTrailer
        };

    })();

    return VideoPageModel;
});
