define(function (require) {
    
    'use strict';

    var $ = require('jquery');

    var Proxy = function () {};

    Proxy.prototype = (function () {
        
        var links = {
            imdb: 'http://www.omdbapi.com/',
            // imdb: 'http://www.omdbapi.com/?i=tt2294629&apikey=4957f146',
            addict: 'http://api.traileraddict.com/?imdb=1403865&count=4&width=680',
            //url: 'http://api.rottentomatoes.com/api/public/v1.0/movies.json?q=GHOSTBUSTERS&page_limit=10&page=1&apikey=phf4rumevrdb658jmc5yut7z',
            clip: 'http://www.imdb.com/title/tt0087332/',
            trailer: 'http://api.rottentomatoes.com/api/public/v1.0/movies/12000/clips.json?apikey=phf4rumevrdb658jmc5yut7z'
        },

        param = {};

        var getDetails = function () {
            return $.ajax({
                url: links.imdb,
                data: { i: 'tt0087332', plot: 'full', r: 'json' },
                method: 'GET',
                dataType: 'jsonp'
            });
        },

        getIMDbId = function () {
            return $.ajax({
                url: links.url,
                method: 'GET',
                dataType: 'jsonp'
            });
        },

        getTrailer = function () {
            return $.ajax({
                url: links.trailer,
                method: 'GET',
                dataType: 'jsonp'
            });
        };

        return {
            getDetails: getDetails,
            getTrailer: getTrailer
        };

    }());

    return Proxy;
});
