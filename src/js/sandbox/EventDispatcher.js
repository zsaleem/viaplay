define(function (require) {
    'use strict';

    var EventDispatcher = function () {};

    EventDispatcher.prototype = (function () {
        var listeners = {};

        var listenEvent = function (eventName, callback) {
            if (!eventName) return;
            if (!callback && typeof callback !== 'function') return;

            listeners[eventName] = callback;
        },

        dispatchEvent = function (eventName, data) {
            if (!eventName) return;

            for (var key in listeners) {
                if (key === eventName && listeners.hasOwnProperty(eventName)) {
                    listeners[eventName](data);
                }
            }
        };

        return {
            listenEvent: listenEvent,
            dispatchEvent: dispatchEvent
        };
    }());

    return EventDispatcher;
});
