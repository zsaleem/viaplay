/*global require:false */
require(['app', 'sandbox/EventDispatcher'], function (App, EventDispatcher) {
    window.Sandbox = {
        Events: new EventDispatcher()
    };

    App.init();
});
